
#include <stdlib.h>
typedef struct {
  double *data;
  int rows;
  int cols;
} Array2d;

void initArray2d(Array2d *array, int x, int y);

double *getValue(Array2d *array, int x, int y);
