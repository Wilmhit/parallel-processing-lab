#include "2darr.h"

double *getValue(Array2d *array, int x, int y) {
  return &array->data[(x * array->cols) + y];
}

void initArray2d(Array2d *array, int x, int y) {
  array->rows = x;
  array->cols = y;
  array->data = (double *)malloc(x * y * sizeof(double));
}
