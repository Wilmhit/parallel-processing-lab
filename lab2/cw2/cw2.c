﻿/************************************************************************
 * DESCRIPTION:
 *   OpenMp Example - Matrix Multiply - C Version
 *   Demonstrates a matrix multiply using OpenMP. Threads share row iterations
 *   according to a predefined chunk size.
 * AUTHOR: Blaise Barney
 * LAST REVISED: 06/28/05
 ******************************************************************************/

#include <2darr.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define NRA 600 /* number of rows in matrix A */
#define NCA 600 /* number of columns in matrix A */
#define NCB 600 /* number of columns in matrix B */

void read_arguments(int argc, char **argv) {
  if (argc == 2) {
    int threads = atoi(argv[1]);
    if (threads > 0 && threads < 500) {
      printf("Setting the number of threads to %d\n", threads);
      omp_set_num_threads(threads);

    } else {
      printf("Wrong number of threads");
      exit(1);
    }
  }
}

void print_thread_info() {
  int tid = omp_get_thread_num();
  if (tid == 0) {
    int nthreads = omp_get_num_threads();
    printf("Starting matrix multiple example with %d threads\n", nthreads);
    printf("Initializing matrices...\n");
  }
}

void fill_matrix(Array2d *array) {
  int chunk = 30;
  int i, j;

#pragma omp for schedule(static, chunk) private(i, j)
  for (i = 0; i < array->rows; i++)
    for (j = 0; j < array->cols; j++) {
      double *val = getValue(array, i, j);
      *val = (double)i + j;
    }
}

int main(int argc, char *argv[]) {
  int tid, i, j, k, chunk, nthreads;
  double start, stop;

  Array2d a, b, c;
  initArray2d(&a, NRA, NCA);
  initArray2d(&b, NCA, NCB);
  initArray2d(&c, NRA, NCB);

  read_arguments(argc, argv);
  chunk = 30;

#pragma omp parallel shared(a, b, c, nthreads, chunk) private(tid, i, j, k)
  {
    print_thread_info();
    tid = omp_get_thread_num();

    fill_matrix(&a);
    fill_matrix(&b);
    fill_matrix(&c);

    printf("Thread %d starting matrix multiply...\n", tid);

#pragma omp barrier

    if (tid == 0) {
      start = omp_get_wtime();
    }

#pragma omp for schedule(static, chunk)
    for (i = 0; i < NRA; i++) {
      printf("Thread=%d did row=%d\n", tid, i);
      for (j = 0; j < NCB; j++)
        for (k = 0; k < NCA; k++) {
          double *aval = getValue(&a, i, k);
          double *bval = getValue(&b, k, j);
          double *cval = getValue(&c, i, j);
          *cval = *aval * *bval;
        }
    }
    stop = omp_get_wtime();
  } /*** End of parallel region ***/

  if (c.cols * c.rows < 1000) { // don't print large arrays
    printf("******************************************************\n");
    printf("Result Matrix:\n");
    for (i = 0; i < c.cols; i++) {
      for (j = 0; j < c.rows; j++) {
        printf("%6.2f   ", *getValue(&c, i, j));
      }
      printf("\n");
    }
  }

  printf("******************************************************\n");
  printf("Done in %fs\n", stop - start);
}
