#include <mpi.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char **argv) {
  int rang, proceses;
  double start, end, max_end = 0;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rang);
  MPI_Comm_size(MPI_COMM_WORLD, &proceses);
  MPI_Barrier(MPI_COMM_WORLD);
  start = MPI_Wtime();
  printf("Starting time of process n. %d %f\n", rang, start);
  if (rang == 0) {
    // Master work
    __asm__("nop");
  } else {
    // slaves work
    __asm__("nop");
  }
  MPI_Barrier(MPI_COMM_WORLD);
  end = MPI_Wtime();
  printf("Ending time of process %d %f\n", rang, end);
  printf("Execution time of process %d %f\n\n\n", rang, end - start);
  MPI_Finalize();
  return 0;
}
