#include <mpi.h>
#include <stdio.h>

#define TAG 0

double recv_all(int nthreads) {
  double sum;
  double recv_buffer;
  MPI_Status status;
  sum = 0;
  for (int i = 0; i < nthreads; i++) {
    MPI_Recv(&recv_buffer, 1, MPI_DOUBLE, i, TAG, MPI_COMM_WORLD, &status);
    sum += recv_buffer;
  }
  return sum;
}

int main(int argc, char *argv[]) {
  long num_steps;
  double step;
  int i, nthreads, thread_num;
  double pi, sum;
  double x;
  int ile;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &thread_num);
  MPI_Comm_size(MPI_COMM_WORLD, &nthreads);

  num_steps = 2000000000;
  step = 1.0 / (double)num_steps;

  sum = 0.0;
  for (i = thread_num; i < num_steps; i += nthreads) {
    x = (i + 0.5) * step;
    sum += 4.0 / (1.0 + x * x);
  }

  MPI_Send(&sum, 1, MPI_DOUBLE, 1, TAG, MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD);
  if (thread_num == 1) {
    sum = recv_all(nthreads);
    pi = sum * step;
    printf(" pi  %f\n", pi);
  }

  printf("Thread %d finished\n", thread_num);
  MPI_Finalize();
}
