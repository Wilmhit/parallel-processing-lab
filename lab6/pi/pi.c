#include <mpi.h>
#include <omp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int print_once(const char *restrict format, ...) {
  int rank;
  va_list ap;

  // From `man 3 printf`
  // If an output error is encountered, a negative value is returned.
  int ret = -1;

  va_start(ap, format);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) {
    // This part is coppied from musl_libc/stdio/printf.c
    ret = vfprintf(stdout, format, ap);
  }

  va_end(ap);
  return ret;
}

void read_thread_num(int argc, char **argv) {
  for (int i = 0; i < argc - 1; i++) {
    if (!strcmp(argv[i], "-t")) {
      int threads = atoi(argv[i + 1]);
      print_once("Found thread argument: %d\n", threads);
      if (threads != 0)
        omp_set_num_threads(threads);
      else
        print_once("Wrong number of threads!\n");
    }
  }
}

int main(int argc, char *argv[]) {
  long num_steps;
  double step;
  int i;
  double start_time, stop_time;
  double pi, sum, reduced;
  double x;
  int processes;
  int this_process;

  MPI_Init(&argc, &argv);
  start_time = MPI_Wtime();
  read_thread_num(argc, argv);

  MPI_Comm_size(MPI_COMM_WORLD, &processes);
  MPI_Comm_rank(MPI_COMM_WORLD, &this_process);

  // needed for pi
  num_steps = 2000000000;
  step = 1.0 / (double)num_steps;
  sum = 0.0;

  // needed for MPI sync
  int steps_for_each_process = num_steps / processes;
  int start = steps_for_each_process * (this_process);
  int finish = start + steps_for_each_process;
  double partial = 0;
#pragma omp parallel for reduction(+ : partial)
  for (i = start; i < finish; i++) {
    if (i == start) {
      printf("Process %d got iterations [%d, %d)\n", this_process, start,
             finish);
    }
    x = (i + 0.5) * step;
    double sum = 4.0 / (1.0 + x * x);
    partial += sum;
  }
  MPI_Reduce(&partial, &reduced, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  stop_time = MPI_Wtime();
  if (this_process == 0) {
    pi = reduced * step;
    stop_time -= start_time;
  }
  print_once(" pi  %f    \n", pi);
  print_once(" time  %f  \n", stop_time);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
