#!/bin/bash

onecore=0

program=./build/pi/pi

for i in {1..8}
do
  start=$( date +%s.%N )
  mpiexec -n 1 --oversubscribe $program --mca opal_warn_on_missing_libcuda 0 -t $i > /dev/null
  stop=$( date +%s.%N )
  runtime=$( echo "$stop - $start" | bc -l )

  if [[ $onecore = 0 ]]
  then
    onecore=$runtime
  fi

  acceleration=$(echo "$onecore / $runtime" | bc -l )
  echo $i $runtime $acceleration
done

