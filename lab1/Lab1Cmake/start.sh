#!/bin/bash

onecore=0


for i in {1..24}
do
  start=$( date +%s.%N )
  $( ./build/MonteCarlo $i > /dev/null )
  stop=$( date +%s.%N )
  runtime=$( echo "$stop - $start" | bc -l )

  if [[ $onecore = 0 ]]
  then
    onecore=$runtime
  fi

  acceleration=$(echo "$onecore / $runtime" | bc -l )
  echo $i $runtime $acceleration
done

