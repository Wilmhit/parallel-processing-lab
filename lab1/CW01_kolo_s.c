// Znajdowanie pola kola metoda Monte Carlo
// wersja sekwencyjna
// polega na zliczaniu punktow spelniajacych r�wnanie x^2+y^2<r^2
// Przyjeto r=1

#include <bits/types.h>
//#include <carloconfig.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#define get_time omp_get_wtime

struct MonteCarlo { // Rezultat algorytmu
  long long suma_pkt;
  long long suma_pkt_w_kole;
};

void print_results(int start, int stop, struct MonteCarlo results) {
  double pi = (double)results.suma_pkt_w_kole / (double)results.suma_pkt * 4;

  printf("\npunkty w kole: %lld\n", results.suma_pkt_w_kole);
  printf("\npunktow wszystkich: %lld\n", results.suma_pkt);
  printf("\npi: %15.12f\n", pi);

  printf("\n czas %d  s \n", stop - start);
}

struct MonteCarlo monte_carlo(int sqr_size) {
  struct MonteCarlo result;
  result.suma_pkt = sqr_size * (long long)sqr_size;

  long long suma_pkt_w_kole = 0;
  long long d_graniczne = sqr_size * (long long)sqr_size;

#pragma omp parallel for reduction(+ : suma_pkt_w_kole)
  for (int x = 0; x < sqr_size; x++) {

    // lokalne zmienne, mozna tez uzyc private()
    long long d_do_srodka = 0;
    long suma_lini = 0;
    for (int y = 0; y < sqr_size; y++) {

      d_do_srodka = x * (long long)x + y * (long long)y;

      if (d_do_srodka < d_graniczne)
        suma_lini++;
    }

    suma_pkt_w_kole += suma_lini;
  }

  result.suma_pkt_w_kole = suma_pkt_w_kole;
  return result;
}

void read_arguments(int argc, char **argv) {
  if (argc == 2) {
    int threads = atoi(argv[1]);
    if (threads > 0 && threads < 500) {
      printf("Setting number of threads to: %d\n", threads);
      omp_set_num_threads(threads);
    } else {
      printf("Wrong number of threads was given\n");
      exit(1);
    }
  }
}

int main(int argc, char **argv) {
  read_arguments(argc, argv);
  int start = get_time();
  struct MonteCarlo algo = monte_carlo(100000);
  int stop = get_time();
  print_results(start, stop, algo);
}
