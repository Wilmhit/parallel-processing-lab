#include <mpi.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char **argv) {
  // Czas jest mierzony zewnetrznie za pomoca komendy `time`
  // int start, stop, czas;
  int ile;
  int i, j;
  double x, y, d;
  double r2, pi, pisuma;
  double s, s1;
  int n_threads, thread_id;

  // start = MPI_Wtime();
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &n_threads);
  MPI_Comm_rank(MPI_COMM_WORLD, &thread_id);

  ile = 100000;
  d = 1.0 / (double)ile;
  s1 = 0;
  for (i = thread_id; i < ile; i += n_threads) {
    for (j = 0; j < ile; j++) {
      x = d * i;
      y = d * j;
      r2 = x * x + y * y;
      if (r2 < 1)
        s1 = s1 + 1;
    }
  }
  s = (double)ile * (double)ile;
  pi = s1 / s * 4;

  MPI_Reduce(&pi, &pisuma, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  MPI_Finalize();
  // stop = MPI_Wtime();

  if (thread_id == 0) {
    printf("\nThreads: %d\n", n_threads);
    printf("\ns1 %15.12f\n", s1);
    printf("\ns %15.12f\n", s);
    printf("\n pole %15.12f\n", pisuma);
    // czas = stop - start;
    // printf("\n czas %d  ms \n", czas);
  }
}
