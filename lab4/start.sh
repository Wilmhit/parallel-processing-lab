#!/bin/bash

onecore=0

program=./build/cw4_4/cw4_4

for i in {1..16}
do
  start=$( date +%s.%N )
  mpiexec -n $i --oversubscribe $program --mca opal_warn_on_missing_libcuda 0 > /dev/null
  stop=$( date +%s.%N )
  runtime=$( echo "$stop - $start" | bc -l )

  if [[ $onecore = 0 ]]
  then
    onecore=$runtime
  fi

  acceleration=$(echo "$onecore / $runtime" | bc -l )
  echo $i $runtime $acceleration
done

