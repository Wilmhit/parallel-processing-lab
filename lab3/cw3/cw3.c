#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

static long num_steps;
double step;

int main(int argc, char *argv[]) {
  int i, nthreads;
  double pi;
  double start, stop, czas;
  int ile;
  ile = 1; // liczba watkow
  num_steps = 200000000;
  if (argc >= 2) {
    ile = atoi(argv[1]);
  }
  step = 1.0 / (double)num_steps;
  omp_set_num_threads(ile);
  start = omp_get_wtime();

#pragma omp parallel for reduction(+ : pi)
  for (i = 0; i < num_steps; i += 1) {
    double x = (i + 0.5) * step;
    pi += 4.0 / (1.0 + x * x);
  }

  stop = omp_get_wtime();
  czas = stop - start;
  printf("\n Watkow  %d     ", ile);
  printf(" pi  %f    ", pi);
  printf(" czas  %f ", czas);
}
